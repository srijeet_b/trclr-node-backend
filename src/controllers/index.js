/* eslint-disable global-require */
module.exports = (wagner) => {
  wagner.factory('BlogController', () => {
    const BlogController = require('./blogController');
    return new BlogController(wagner);
  });
};
