/* eslint-disable global-require */
module.exports = (wagner) => {
  wagner.factory('apicall', () => {
    const ApiCall = require('./apicall');
    return new ApiCall(wagner);
  });
};
