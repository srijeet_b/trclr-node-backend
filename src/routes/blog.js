/* eslint-disable global-require */
module.exports = (app, wagner) => {
  const blogController = wagner.get('BlogController');
  const { param } = require('express-validator');

  app.get(
    '/api/blogs/:lowerlimit/:upperlimit',
    [
      param('upperlimit', 'Upperlimit must be valid but is optional entity').optional(),
      param('lowerlimit', 'Upperlimit must be valid but is optional entity').optional(),
    ],
    async (req, res) => {
      const blogs = await blogController.getBlogs(req.params.lowerlimit, req.params.upperlimit);
      return res.status(blogs.code).json(blogs.data);
    },
  );
};
