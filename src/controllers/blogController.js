const SITE_URL = 'https://public-api.wordpress.com/rest/v1.1/sites/truecaller.blog/posts/?number=';

class BlogController {
  constructor(wagner) {
    this.wagner = wagner;
    this.apicall = this.wagner.get('apicall');
    this.siteurl = SITE_URL;
  }

  blogpostsfieldfilter(post) {
    this.post = post;
    const {
      author,
      ID,
      title,
      // eslint-disable-next-line camelcase
      short_URL,
      categories,
    } = this.post;
    return {
      author,
      ID,
      title,
      short_URL,
      categories,
    };
  }

  async getBlogs(lower = 0, upper = 25) {
    try {
      let blogs = [];
      blogs = await this.apicall.getAPI(`${this.siteurl}${upper}`);
      if (blogs.data.found === 0 || (blogs.data.posts && blogs.data.posts.length === 0)) {
        return { code: 204, data: {} };
      }
      blogs = blogs.data.posts;
      if (blogs.length) blogs = blogs.map((b) => this.blogpostsfieldfilter(b));
      if (lower === 0) {
        return { code: 200, data: blogs };
      }
      blogs = blogs.slice(lower, upper);
      return { code: 200, data: blogs };
    } catch (error) {
      error.status = error.status || 400;
      return { code: error.status, data: error.message };
    }
  }
}

module.exports = BlogController;
