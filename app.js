const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const wagner = require('wagner-core');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const app = express();
app.use(helmet());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.get('/', (_, res) => res.redirect('/apidocs'));
app.use('/apidocs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

require('./src/utils/helpers')(wagner);
require('./src/controllers')(wagner);
require('./src/routes')(app, wagner);

module.exports = app;
