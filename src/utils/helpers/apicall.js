const axios = require('axios');

class ApiCaller {
  constructor(wagner) {
    this.wagner = wagner;
    this.axios = axios;
  }

  async getAPI(url) {
    try {
      const results = await this.axios.get(url);
      return results;
    } catch (err) {
      throw Error('API FETCH FAILED!');
    }
  }
}

module.exports = ApiCaller;
